variable "token_ya" {
  type    = string
  default = "................" #OAuth-токен для работы с Yandex Cloud https://cloud.yandex.ru/ru/docs/iam/concepts/authorization/oauth-token
}

variable "cloud_id_ya" {
  type    = string
  default = "................" #Идентификатор облака https://cloud.yandex.ru/ru/docs/resource-manager/operations/cloud/get-id
}

variable "folder_id_ya" {
  type    = string
  default = "................" #Идентификатор каталога https://cloud.yandex.ru/docs/resource-manager/operations/folder/get-id
}
