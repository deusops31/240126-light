terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  token     = var.token_ya
  cloud_id  = var.cloud_id_ya
  folder_id = var.folder_id_ya
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "game-vm-1" {
  name        = "game-vm-1"
  platform_id = "standard-v2"
  resources {
    cores         = 2
    memory        = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = "fd866kfu2hbk46j2e21q" #Debian 12
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./main.yml")}"
  }

  scheduling_policy {
    preemptible = true #Создание прерываемой ВМ для экономии баланса при обучении
  }

}
resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}
